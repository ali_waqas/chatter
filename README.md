# Messaging System #

This chat system is written using Laravel, Live Demo is available at http://codelocker.xyz

### Technologies ###

* Laravel 5.4
* PHP 5.6
* MongoDb

### How to setup? ###

All the installations can be completed using composer, once cloned and inside the directory. run "composer install". Once the installation is completed run "php artisan serve", application can be accessed via http://127.0.0.1:8000

### How it works ###

Application logic is inside the app/Http/Controllers/*.

### Supported Methods ###

All supported routes/methods can be found inside routes/web.php

### room/create
**Method: POST**

**Parameters: **
*room_name(required) 

### rooms/list
**Method: GET**

### user/create
**Method: POST**

**Parameters: **
*first_name(required) 
*last_name(required) 
*username(required | unique) 
*email(required) 
*role(required | in ("General","Admin"))

### user/{userID}/messages
**Method: GET**
Lists the private messages of a user.

### join/room/
**Method: POST**
To signup a user to a specific chatroom
**Parameters: **
*user_id(required) 
*room_id(required) 

### send/private/message
**Method: POST**
Send a private message to a user. Using sender and recipient id as valid users.
To signup a user to a specific chatroom
**Parameters: **
*sender_id(required) 
*recipient_id(required | Valid User ID) 
*message(required) 
*deliver_at(Valid DateTime (Y-m-d H:i:s)) // Only Admins can schedule a message

### send/room/message
**Method: POST**
Send a message to a chat room.
**Parameters: **
*sender_id(required) 
*recipient_id(required | Valid Room ID) 
*message(required) 
*deliver_at( Optional - Valid DateTime (Y-m-d H:i:s)) // Only Admins can schedule a message

### send/rooms/message
**Method: POST**
Send a message to all chat rooms.
**Parameters: **
*sender_id(required) 
*message(required) 
*deliver_at( Optional - Valid DateTime (Y-m-d H:i:s)) // Only Admins can schedule a message




### chatroom/{chatRoomID}/users
**Method: GET**
Lists all the users currently available in a chat room.

### chatroom/{chatRoomID}/messages
**Method: GET**
Fetches all the messages sent to a specific chat room.