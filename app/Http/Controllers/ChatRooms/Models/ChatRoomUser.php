<?php 
namespace App\Http\Controllers\ChatRooms\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ChatRoomUser extends Eloquent {
	protected $connection = 'mongodb';
    protected $collection = 'foo_chat_room_users';
    protected $fillable = [
    						'chat_room_id',
    						'user_id',
    						'left_at'
    					  ];
}

?>