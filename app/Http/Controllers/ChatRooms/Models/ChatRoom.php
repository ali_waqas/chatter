<?php 
namespace App\Http\Controllers\ChatRooms\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ChatRoom extends Eloquent {
	protected $connection = 'mongodb';
    protected $collection = 'foo_chat_rooms';
    protected $fillable = [
    						'room_name',
    						'max_user',
    						'active_users'
    					  ];
}

?>