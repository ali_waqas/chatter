<?php 
namespace App\Http\Controllers\ChatRooms\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MessagePool extends Eloquent {
	protected $connection = 'mongodb';
    protected $collection = 'foo_message_pool';
    protected $dates = ['deliver_at'];
    protected $fillable = [
    						'message',
    						'message_type',
    						'recipient_id',
    						'recipient_type',
    						'sender_id',
    						'sender_type',
    						'sender',
    						'deliver_at'
    					  ];
}

?>