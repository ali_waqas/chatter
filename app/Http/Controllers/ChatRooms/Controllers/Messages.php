<?php
namespace App\Http\Controllers\ChatRooms\Controllers;
use App;
use Request;
use Response;
use Validator;
use Exception;
use DateTime;
use App\Http\Controllers\ChatRooms\Models\ChatRoom;
use App\Http\Controllers\ChatRooms\Models\ChatRoomUser;
use App\Http\Controllers\ChatRooms\Models\MessagePool;
use App\Http\Controllers\Users\Models\User;

use App\Http\Controllers\Controller;

class Messages{

    private $message;
    private $message_type = null;
    private $recipient = null;
    private $sender = null;
    private $deliver_at = null;

    public function __construct()
    {
        $verifier = App::make('validation.presence')->setConnection('mongodb');
    }

    public function setMessageType($message_type){
        $this->message_type = $message_type;
        return $this;
    }

    public function setRecipientType($recipient_type){
        $this->recipient_type = $recipient_type;
        return $this;
    }

    public function setRecipient($recipient){
        $this->recipient = $recipient;
        return $this;
    }

    public function setSender($sender){
        $this->sender = $sender;
        return $this;
    }

    public function scheduleFor($dateTime){
        $this->deliver_at = $dateTime;
        return $this;
    }

    public function getDeliveryDateTime(){
        if($this->deliver_at){
            return new DateTime($this->deliver_at);
        }else{
            return new DateTime();
        }
    }

    public function send($message){
        if(!$this->message_type)
            throw new Exception("Message type not defined.", 1);
        if(!$this->sender)
            throw new Exception("Invalid Sender", 1);
        if(!$this->recipient)
            throw new Exception("Invalid Recipient", 1);
        if(empty(trim($message)))
            throw new Exception("You cannot send an empty message", 1);

        switch($this->message_type){
            case "PM":
                $Message = [
                                'recipient_id'=>$this->recipient->id,
                                'sender_id'=>$this->sender->id,
                                'message_type'=>'PM',
                                'message'=>$message,
                                'deliver_at'=>$this->getDeliveryDateTime(),
                                'sender'=>$this->sender->toArray()
                            ];
            break;
            case "CRM": //Chat Room Message

                $UserHasJoined = ChatRoomUser::where('room_id',$this->recipient->id)->where('user_id',$this->sender->id)->first();
                if($this->sender->role=='Regular' && !$UserHasJoined)
                    throw new Exception("You have not joined this room, please join the in order to send message to room", 1);

                $Message = [
                                'recipient_id'=>$this->recipient->id,
                                'sender_id'=>$this->sender->id,
                                'message_type'=>'CRM',
                                'message'=>$message,
                                'deliver_at'=>$this->getDeliveryDateTime(),
                                'sender'=>$this->sender->toArray()
                            ];
            break;
            default:
            break;
        }
        if(is_array($Message)){
            $MessagePool = new MessagePool($Message);
            $MessagePool->save();
            $this->sender->increment('messages_sent');
            return $MessagePool->toArray();
        }
    }  

    public function sendPrivateMessageToUser(){
        $data = Request::all();
        $validationRules = [
                            'sender_id'=>'required|exists:foo_users,_id',
                            'recipient_id'=>'required|exists:foo_users,_id',
                            'message'=>'required',
                            'deliver_at'=>'sometimes|required|date_format:Y-m-d H:i:s'
                           ];
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $senderID = $data['sender_id'];
            $recipientID = $data['recipient_id'];
            $message = $data['message'];
            $Sender = User::where('_id',$senderID)->first();
            $Recipient = User::where('_id',$recipientID)->first();
            $Message = $this->setMessageType('PM')
                            ->setSender($Sender)
                            ->setRecipient($Recipient);
            if($Sender->role=='Admin' && isset($data['deliver_at'])){ // Only Admins can schedule a message
                $Message = $Message->scheduleFor($data['deliver_at']);
            }
            $Message = $Message->send($message);
            $content = ['action'=>true,'Message'=>$Message];
            $statusCode = 200;
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }

    public function sendMessageToChatRoom(){
        $data = Request::all();
        $validationRules = [
                            'sender_id'=>'required|exists:foo_users,_id',
                            'recipient_id'=>'required|exists:foo_chat_rooms,_id',
                            'message'=>'required',
                            'deliver_at'=>'sometimes|required|date_format:Y-m-d H:i:s'
                           ];
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $senderID = $data['sender_id'];
            $recipientID = $data['recipient_id'];
            $message = $data['message'];
            $Sender = User::where('_id',$senderID)->first();
            $Recipient = ChatRoom::where('_id',$recipientID)->first();
            $Message = $this->setMessageType('CRM')
                            ->setSender($Sender)
                            ->setRecipient($Recipient);
            if($Sender->role=='Admin' && isset($data['deliver_at'])){ // Only Admins can schedule a message
                $Message = $Message->scheduleFor($data['deliver_at']);
            }
            $Message = $Message->send($message);
            $content = ['action'=>true,'Message'=>$Message];
            $statusCode = 200;
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }

    public function sendMessageToAllChatRooms(){
        $data = Request::all();
        $validationRules = [
                            'sender_id'=>'required|exists:foo_users,_id',
                            'message'=>'required',
                            'deliver_at'=>'sometimes|required|date_format:Y-m-d H:i:s'
                           ];
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $senderID = $data['sender_id'];

            $message = $data['message'];
            $Sender = User::where('_id',$senderID)->first();
            if($Sender->role=='Admin'){
                $ChatRooms = ChatRoom::all();
                foreach($ChatRooms as $ChatRoom){
                    $Message = $this->setMessageType('CRM')
                                    ->setSender($Sender)
                                    ->setRecipient($ChatRoom);
                    if($Sender->role=='Admin' && isset($data['deliver_at'])){ // Only Admins can schedule a message
                        $Message = $Message->scheduleFor($data['deliver_at']);
                    }
                    $Message = $Message->send($message);
                }
                $content = ['action'=>true,'Message'=>$Message];
                $statusCode = 200;
            }else{
                $content = ['action'=>false,'Message'=>'Unable to send message to chat rooms, you are not an admin.'];
                $statusCode = 400;
            }
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }

    public function getPrivateMessagesByUserId($UserID){
        $PrivateMessages = MessagePool::where('recipient_id',$UserID)
                                    ->where('message_type','PM')
                                    ->where('deliver_at','<=',new DateTime)
                                    ->select(['sender.first_name','sender.last_name','sender.username','sender._id','message_type','message','created_at'])
                                    ->orderBy('created_at','DESC')
                                    ->get();
        return $PrivateMessages;
    }

    public function getChatRoomMessagesByChatRoomId($chatRoomID){
        $ChatRoomMessages = MessagePool::where('recipient_id',$chatRoomID)
                                        ->where('message_type','CRM')
                                        ->where('deliver_at','<=',new DateTime)
                                        ->select(['sender.first_name','sender.last_name','sender.username','sender._id','message_type','message','created_at'])
                                        ->orderBy('created_at','DESC')
                                        ->get();
        return $ChatRoomMessages;
    }

}


