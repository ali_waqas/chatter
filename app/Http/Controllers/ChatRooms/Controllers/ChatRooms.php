<?php
namespace App\Http\Controllers\ChatRooms\Controllers;
use App;
use Request;
use Response;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ChatRooms\Models\ChatRoom;
use App\Http\Controllers\ChatRooms\Models\ChatRoomUser;
use App\Http\Controllers\ChatRooms\Controllers\Messages;
use App\Http\Controllers\ChatRooms\Models\MessagePool;
use App\Http\Controllers\Users\Models\User;

class ChatRooms extends Controller{

    private $Messages;
    public function __construct(Messages $Messages)
    {
        $this->Messages = $Messages;
    }

    public function create(){
        $data = Request::all();
        $validationRules = [
                                'room_name'=>'required|unique:foo_chat_rooms,room_name',
                                'max_users'=>'required|numeric'
                           ];
        
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $ChatRoom = new ChatRoom;
            $ChatRoom->room_name = $data['room_name'];
            $ChatRoom->max_users = $data['max_users'];
            $ChatRoom->active_users = 0;
            try{
                $ChatRoom->save(); 
                $content = ['action'=>true,'message'=>'Room created successfully.'];
                $statusCode = 200;
            }catch(Exception $e){
                $content = ['action'=>false,'message'=>$e->getMessage()];
                $statusCode = 401;
            }
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }

    public function joinRoom(){
        $data = Request::all();
        $userID = $data['user_id'];
        $chatRoomID = $data['room_id'];
        $validationRules = [
                                'user_id'=>'required|exists:foo_users,_id',
                                'room_id'=>'required|exists:foo_chat_rooms,_id',
                           ];
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $ChatRoomUserExists = ChatRoomUser::where('user_id',$userID)->where('room_id',$chatRoomID)->first();
            if(!$ChatRoomUserExists):
                $User = User::where('_id',$userID)->select(['first_name','last_name','email','username','role','_id'])->first()->toArray();
                $ChatRoom = ChatRoom::where('_id',$chatRoomID)->first();
                $ChatRoomActiveUsers = ChatRoomUser::where('room_id',$chatRoomID)->count();
                if($ChatRoomActiveUsers<$ChatRoom->max_users):
                    $ChatRoomUser = new ChatRoomUser;
                    $ChatRoomUser->user_id = $userID;
                    $ChatRoomUser->room_id = $chatRoomID;
                    $ChatRoomUser->user = $User;
                    try{
                        $ChatRoomUser->save(); 
                        ChatRoom::where('_id',$chatRoomID)->increment('active_users',1);
                        $content = ['action'=>true,'message'=>'Room joined successfully'];
                        $statusCode = 200;
                    }catch(Exception $e){
                        $content = ['action'=>false,'message'=>$e->getMessage()];
                        $statusCode = 400;
                    }
                else:
                    $content = ['action'=>false,'message'=>'Room is maxed out, you cannot join this room right now, please try again later.'];
                    $statusCode = 403;
                endif;
            else:
                $content = ['action'=>true,'message'=>'You have already joined this room'];
                $statusCode = 200;
            endif;
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }

    public function listChatRoomUsers($chatRoomID){
        $data = ['room_id'=>$chatRoomID];
        $validationRules = [
                            'room_id'=>'required|exists:foo_chat_rooms,_id',
                           ];
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $ChatRoomUsers = ChatRoomUser::where('room_id',$chatRoomID)->select(['user.first_name','user.last_name','user.username','user._id'])->get();
            $content = ['action'=>true,'ChatRoomUsers'=>$ChatRoomUsers];
            $statusCode = 200;
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }

    public function listChatRoomMessages($chatRoomID){
        $data = ['room_id'=>$chatRoomID];
        $validationRules = [
                            'room_id'=>'required|exists:foo_chat_rooms,_id',
                           ];
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $ChatRoomMessages = $this->Messages->getChatRoomMessagesByChatRoomId($chatRoomID);
            $content = ['action'=>true,'ChatRoomMessages'=>$ChatRoomMessages];
            $statusCode = 200;
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }
}
