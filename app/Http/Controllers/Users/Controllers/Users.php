<?php
namespace App\Http\Controllers\Users\Controllers;
use App;
use Illuminate\Validation\Rule;
use Request;
use Response;
use Validator;
use App\Http\Controllers\Users\Models\User;
use App\Http\Controllers\ChatRooms\Models\MessagePool;
use App\Http\Controllers\ChatRooms\Controllers\Messages;

use App\Http\Controllers\Controller;

class Users extends Controller
{
    private $Messages;
    public function __construct(Messages $Messages)
    {
        $this->Messages = $Messages;
    }

    public function create(){
        $data = Request::all();
        $validationRules = [
                                'first_name'=>'required',
                                'last_name'=>'required',
                                'username'=>'required|unique:foo_users,username',
                                'email'=>'required|email',
                                'role'=>['required',Rule::in(['Regular', 'Admin'])]
                           ];
        $verifier = App::make('validation.presence')->setConnection('mongodb');
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $User = new User;
            $User->first_name = $data['first_name'];
            $User->last_name = $data['last_name'];
            $User->username = $data['username'];
            $User->email = $data['email'];
            $User->role = $data['role'];
            $User->messages_sent = 0;
            try{
                $User->save(); 
                $content = ['action'=>true,'message'=>'User created successfully.','User'=>$User->toArray()];
                $statusCode = 200;
            }catch(Exception $e){
                $content = ['action'=>false,'message'=>$e->getMessage()];
                $statusCode = 401;
            }
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }

    public function listMessages($userID){
        $data = ['user_id'=>$userID];
        $validationRules = [
                            'user_id'=>'required|exists:foo_users,_id',
                           ];
        $Validation = Validator::make($data,$validationRules);
        if($Validation->passes()){
            $PrivateMessages = $this->Messages->getPrivateMessagesByUserId($userID);
            $content = ['action'=>true,'PrivateMessages'=>$PrivateMessages];
            $statusCode = 200;
        }else{
            $content = ['action'=>false,'message'=>'Validation Failed','errors'=>$Validation->errors()];
            $statusCode = 400;
        }
        return Response::make($content, $statusCode);
    }
}
