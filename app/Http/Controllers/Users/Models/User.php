<?php 
namespace App\Http\Controllers\Users\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class User extends Eloquent {
	protected $connection = 'mongodb';
    protected $collection = 'foo_users';
    protected $fillable = [
    						'first_name',
    						'last_name',
    						'username',
    						'email',
    						'role',
    						'messages_sent'
    					  ];
}

?>