<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', function(){
	echo '<h3><center>Please use API doc to make requests.</center></h3>';
});
Route::post('room/create', 'ChatRooms\Controllers\ChatRooms@create');
Route::any('rooms/list', 'ChatRooms\Controllers\ChatRooms@listRooms');
Route::post('user/create', 'Users\Controllers\Users@create');
Route::get('user/{userID}/messages', 'Users\Controllers\Users@listMessages');
Route::post('join/room/','ChatRooms\Controllers\ChatRooms@joinRoom');
Route::post('send/private/message','ChatRooms\Controllers\Messages@sendPrivateMessageToUser');
Route::post('send/room/message','ChatRooms\Controllers\Messages@sendMessageToChatRoom');
Route::post('send/rooms/message','ChatRooms\Controllers\Messages@sendMessageToAllChatRooms');
Route::get('chatroom/{chatRoomID}/users','ChatRooms\Controllers\ChatRooms@listChatRoomUsers');
Route::get('chatroom/{chatRoomID}/messages','ChatRooms\Controllers\ChatRooms@listChatRoomMessages');
