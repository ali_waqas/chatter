<?php
    // Database config/connection should be moved to config.php
	bootstrap(); 
	// Bootstrap will perform different checks before loading the actual app. It will also connect with DB Using PDO.
	
	// I will be Adopting MVC approach. But for the demo all of the code will be inside the same file.
	// Model for videos. Our Model uses PDO. 
	class Video extends Model{ 
		protected $connection = 'Mysql';
		protected $table = 'videos';
	}

	// Controller that correspondes to the view video request.
	class Videos extends Controller{
		public function __construct(){
			// Any required business logic goes here.
			// Login Check etc
		}

		public function view($videoID){
			$videoID = (int)$videoID;
			if(!$videoID)
				throw new Exception("Invalid Video.", 1);
			// "where" performs all sort of required SQL Inject checks.
			$Video = Video::where('id',$videoID)->first(); // Returns a video object.
			if($Video): 
				return view('video.html')->with('Video',$Video);
			else:
				return view('video_not_found.html');
			endif;
		}
	}	
?>

// video.php
echo '<h3>{{$Video->title . '</h3>
echo '<div>'.$Video->description.'</div>';
echo 'You are viewing <a href="{{URL::to('video?id='.$Video->id)}}">This video</a>';